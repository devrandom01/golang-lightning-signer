package server

import (
	"bytes"
	"log"

	"github.com/btcsuite/btcd/btcec"
	"github.com/btcsuite/btcutil"

	"gitlab.com/lightning-signer/lightning-signer/lnkey"
	"gitlab.com/lightning-signer/lightning-signer/lnscript"
	. "gitlab.com/lightning-signer/lightning-signer/logging"
)

func validateRemoteToLocal(basePoints lnkey.Basepoints, remoteCommitPoint btcec.PublicKey, info lnscript.CommitmentInfo) error {
	// to_local from the point of view of the peer, since we are looking at a remote commitment tx
	revocationPubKey := lnkey.DeriveRevocationPubkey(basePoints.Revocation, &remoteCommitPoint)
	match := bytes.Equal(info.RevocationKey.SerializeCompressed(), revocationPubKey.SerializeCompressed())
	if !match {
		return ValidationError("Commitment/Revocation")
	}
	if info.Delay < policy.MinimumDelay || info.Delay > policy.MaximumDelay {
		log.Println(
			"policy.MinimumDelay =", policy.MinimumDelay,
			" log.Delay =", info.Delay,
			" policy.MaximumDelay =", policy.MaximumDelay)
		return ValidationError("Commitment/To self delay")
	}
	return nil
}

func validateRemoteToRemote(shaseed []byte, n uint64, info lnscript.CommitmentInfo,
	basePoints lnkey.Basepoints, staticRemoteKey bool) error {
	commitPoint := lnkey.PerCommitmentPoint(shaseed, n)
	DebugIt("per-commit point", lnkey.DumpRawPubKey(commitPoint))
	var actualAddress = info.ToRemoteAddress.ScriptAddress()
	if staticRemoteKey {
		pubkeyHashStatic := btcutil.Hash160(basePoints.Payment.SerializeCompressed())
		if !bytes.Equal(actualAddress, pubkeyHashStatic) {
			//noinspection GoBoolExpressions
			return ValidationError("Commitment/Payment")
		}
	} else {
		paymentPubKey := lnkey.TweakPubKey(basePoints.Payment, commitPoint)
		DebugIt("payment pubkey", lnkey.DumpRawPubKey(paymentPubKey))
		pubkeyHash := btcutil.Hash160(paymentPubKey.SerializeCompressed())
		if !bytes.Equal(actualAddress, pubkeyHash) {
			return ValidationError("Commitment/Payment")
		}
	}
	return nil
}
