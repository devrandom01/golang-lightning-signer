package lnkey

import (
	"crypto/sha256"
	"encoding/hex"
	"github.com/btcsuite/btcd/btcec"
	"golang.org/x/crypto/hkdf"
	"io"
	"math/big"

	. "gitlab.com/lightning-signer/lightning-signer/logging"
	"gitlab.com/lightning-signer/lightning-signer/shachain"
)

type Secrets struct {
	FundingPrivkey                *btcec.PrivateKey
	RevocationBasepointSecret     []byte
	PaymentBasepointSecret        []byte
	HTLCBasepointSecret           []byte
	DelayedPaymentBasepointSecret []byte
}

type Basepoints struct {
	Revocation     *btcec.PublicKey
	Payment        *btcec.PublicKey
	HTLC           *btcec.PublicKey
	DelayedPayment *btcec.PublicKey
}

type Keys struct {
	Funding        []byte // sizeof(struct privkey) = 32
	Revocation     []byte // sizeof(struct privkey) = 32
	HTLC           []byte // sizeof(struct privkey) = 32
	Payment        []byte // sizeof(struct privkey) = 32
	DelayedPayment []byte // sizeof(struct privkey) = 32
	ShaSeed        []byte // sizeof(struct sha256)  = 32
}

func panicOnError(e error) {
	if e != nil {
		panic(e)
	}
}

func deriveKeys(seed []byte, keys *Keys) {
	sz := 32 * 6 // sizeof(Keys)
	salt0 := make([]byte, 0)
	buf := HkdfSha256(seed, salt0, []byte("c-lightning"), sz)
	ndx := 0
	keys.Funding = buf[ndx : ndx+32]
	ndx += 32
	keys.Revocation = buf[ndx : ndx+32]
	ndx += 32
	keys.HTLC = buf[ndx : ndx+32]
	ndx += 32
	keys.Payment = buf[ndx : ndx+32]
	ndx += 32
	keys.DelayedPayment = buf[ndx : ndx+32]
	ndx += 32
	keys.ShaSeed = buf[ndx : ndx+32]
	ndx += 32
	if ndx != sz {
		panic("deriveKeys: indexing problem")
	}
}

func DeriveFundingKey(seed []byte) (*btcec.PrivateKey, *btcec.PublicKey) {
	var keys Keys
	deriveKeys(seed, &keys)
	return btcec.PrivKeyFromBytes(btcec.S256(), keys.Funding)
}

func pubkeyFromPrivkey(pkBytes []byte) *btcec.PublicKey {
	_, pubKey := btcec.PrivKeyFromBytes(btcec.S256(), pkBytes)
	return pubKey
}

// Derive funding pubkey, basepoints, secret and SHA chain seed from channel seed
// Any of the output paramters can be nil if caller is not interested in that item
// TODO - switch to return values to be more golang idiomatic
func DeriveBasepoints(
	seed []byte,
	fundingPubkeyPtr **btcec.PublicKey,
	basepointsPtr *Basepoints,
	secretsPtr *Secrets,
	shaseedPtr *[]byte) error {

	var keys Keys
	deriveKeys(seed, &keys)
	DebugIt("deriveBasepoints:keys", keys)

	if secretsPtr != nil {
		privKey, _ := btcec.PrivKeyFromBytes(btcec.S256(), keys.Funding)
		secretsPtr.FundingPrivkey = privKey
		secretsPtr.RevocationBasepointSecret = keys.Revocation
		secretsPtr.PaymentBasepointSecret = keys.Payment
		secretsPtr.HTLCBasepointSecret = keys.HTLC
		secretsPtr.DelayedPaymentBasepointSecret = keys.DelayedPayment
	}

	if fundingPubkeyPtr != nil {
		*fundingPubkeyPtr = pubkeyFromPrivkey(keys.Funding)
	}

	if basepointsPtr != nil {
		basepointsPtr.Revocation = pubkeyFromPrivkey(keys.Revocation)
		basepointsPtr.Payment = pubkeyFromPrivkey(keys.Payment)
		basepointsPtr.HTLC = pubkeyFromPrivkey(keys.HTLC)
		basepointsPtr.DelayedPayment = pubkeyFromPrivkey(keys.DelayedPayment)
	}

	if shaseedPtr != nil {
		*shaseedPtr = keys.ShaSeed
	}

	return nil
}

func HkdfSha256(secret, salt, info []byte, outsz int) []byte {
	hashfun := sha256.New
	retval := make([]byte, outsz)
	hkdfrdr := hkdf.New(hashfun, secret, salt, info)
	_, err := io.ReadFull(hkdfrdr, retval)
	panicOnError(err)
	return retval
}

func computeCommitmentPoint(commitSecret []byte) *btcec.PublicKey {
	x, y := btcec.S256().ScalarBaseMult(commitSecret)

	return &btcec.PublicKey{
		X:     x,
		Y:     y,
		Curve: btcec.S256(),
	}
}

// PerCommitmentPoint generates a commitment point given a commitment
// secret. The commitment point for each state is used to randomize each key in
// the key-ring and also to used as a tweak to derive new public+private keys
// for the state.
func PerCommitmentPoint(shaseed []byte, n uint64) *btcec.PublicKey {
	producer, err := shachain.NewRevocationProducerFromBytes(shaseed)
	panicOnError(err)
	secret0, err := producer.AtIndex(n)
	panicOnError(err)
	commitmentPoint := computeCommitmentPoint(secret0[:])
	return commitmentPoint
}

// Reverse the endianness of a byte slice.
func revEndian(seq []byte) []byte {
	buf := make([]byte, len(seq))
	for ii := 0; ii < len(buf); ii++ {
		buf[ii] = seq[len(seq)-ii-1]
	}
	return buf
}

// Parse a public key from c-lightning specific serialization
func PubKeyFromRaw(point []byte) btcec.PublicKey {
	xx := new(big.Int).SetBytes(revEndian(point[:32]))
	yy := new(big.Int).SetBytes(revEndian(point[32:]))
	return btcec.PublicKey{Curve: btcec.S256(), X: xx, Y: yy}
}

// Dump a public key in c-lightning specific format
func DumpRawPubKey(key *btcec.PublicKey) string {
	x := revEndian(key.X.Bytes())
	y := revEndian(key.Y.Bytes())
	return hex.EncodeToString(x) + hex.EncodeToString(y)
}

// From github.com/lightningnetwork/lnd/brontide/noise.go:
// ecdh performs an ECDH operation between pub and priv. The returned value is
// the sha256 of the compressed shared point.
func Ecdh(pub *btcec.PublicKey, priv *btcec.PrivateKey) []byte {
	s := &btcec.PublicKey{}
	x, y := btcec.S256().ScalarMult(pub.X, pub.Y, priv.D.Bytes())
	s.X = x
	s.Y = y

	h := sha256.Sum256(s.SerializeCompressed())
	return h[:]
}
