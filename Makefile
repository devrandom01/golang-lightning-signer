PKG := gitlab.com/lightning-signer/lightning-signer

COMMIT := $(shell git describe --always --abbrev=8 --dirty)
LDFLAGS := -ldflags "-X $(PKG).Commit=$(COMMIT)"

GOBUILD := GO111MODULE=on go build -v

RM := rm -f

DEV_TAGS := $(if ${tags},$(DEV_TAGS) ${tags},$(DEV_TAGS))

GENERATED := remotesigner/remotesigner.pb.go

default: scratch

all: scratch

build: $(GENERATED)
	$(GOBUILD) -tags="$(DEV_TAGS)" -o liposigd $(LDFLAGS) $(PKG)/cmd/liposigd

scratch: build

clean:
	$(RM) ./liposigd
	$(RM) $(GENERATED)

remotesigner/remotesigner.pb.go: remotesigner/remotesigner.proto
	cd ./remotesigner; ./regen.sh

.PHONY: default all build scratch clean
