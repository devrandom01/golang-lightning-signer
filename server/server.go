package server

import (
	"bytes"
	"context"
	"encoding/hex"
	"fmt"
	"log"

	"github.com/btcsuite/btcd/btcec"
	"github.com/btcsuite/btcd/chaincfg"
	"github.com/btcsuite/btcd/txscript"
	"github.com/btcsuite/btcd/wire"
	"github.com/btcsuite/btcutil/hdkeychain"

	"gitlab.com/lightning-signer/lightning-signer/lnkey"
	"gitlab.com/lightning-signer/lightning-signer/lnscript"
	. "gitlab.com/lightning-signer/lightning-signer/logging"
	. "gitlab.com/lightning-signer/lightning-signer/remotesigner"
)

type SignerService struct {
	// Mapping of local node to node information
	// TODO persist this information
	Nodes NodeMap
}

func NewSignerService() (*SignerService, error) {
	log.Println("SignerService started")
	return &SignerService{
		Nodes: NodeMap{},
	}, nil
}

func (svc *SignerService) findNode(nodeId *NodeId) *NodeState {
	node, ok := svc.Nodes[MakeNodeIdVal(nodeId.Data)]
	if !ok {
		panic(fmt.Sprintf("node data for %v not found", nodeId.Data))
	}
	return node
}

func (svc *SignerService) findNodeChannel(nodeId *NodeId, channelNonce []byte) (*NodeState, *ChannelState) {
	node, ok := svc.Nodes[MakeNodeIdVal(nodeId.Data)]
	if !ok {
		panic(fmt.Sprintf("node data for %v not found", nodeId.Data))
	}
	cn := MakeChannelNonce(channelNonce)
	channel, ok := node.Channels[cn]
	if !ok {
		// ChannelState doesn't exist yet, create new one.
		channel = &ChannelState{}
		node.Channels[cn] = channel
	}
	return node, channel
}

func (svc *SignerService) Ping(ctx context.Context,
	req *PingRequest) (*PingReply, error) {
	LogIt("Ping req", req)

	rsp := &PingReply{Message: req.Message}
	LogIt("Ping rsp", rsp)
	return rsp, nil
}

func (svc *SignerService) Init(ctx context.Context,
	req *InitRequest) (*InitReply, error) {
	LogIt("Init req", req)

	// Derivation here matches c-lightning, but may change
	salt4 := make([]byte, 4)

	// Generate the node private key and nodeid.
	pkBytes := lnkey.HkdfSha256(req.HsmSecret.Data, salt4, []byte("nodeid"), 32)
	nodePrivKey, nodePubKey := btcec.PrivKeyFromBytes(btcec.S256(), pkBytes)
	nodeIdSlice := nodePubKey.SerializeCompressed()
	nodeId := MakeNodeIdVal(nodeIdSlice)

	// Generate the BIP32 extended key.
	var chainParams = &chaincfg.MainNetParams
	if req.Chainparams.Testnet {
		chainParams = &chaincfg.TestNet3Params
	}
	bip32Seed := lnkey.HkdfSha256(req.HsmSecret.Data, salt4, []byte("bip32 seed"), 32)
	masterExtKey, err := hdkeychain.NewMaster(bip32Seed, chainParams)
	PanicOnError(err)
	childExtKey, err := masterExtKey.Child(0)
	PanicOnError(err)
	bip32Key, err := childExtKey.Child(0)
	PanicOnError(err)

	// Generate the Channel Seed Base
	salt0 := make([]byte, 0)
	csb := lnkey.HkdfSha256(req.HsmSecret.Data, salt0, []byte("peer seed"), 32)

	// Create the node record and store for future use.
	log.Println("Created node for", hex.EncodeToString(nodeId[:]))
	svc.Nodes[nodeId] = &NodeState{
		NodeChainParams: req.Chainparams,
		Secret:          req.HsmSecret.Data,
		PrivKey:         nodePrivKey,
		BIP32Key:        bip32Key,
		ChannelSeedBase: csb,
		Channels:        ChannelMap{},
	}

	rsp := &InitReply{SelfNodeId: &NodeId{Data: nodeId[:]}}
	LogIt("Init rsp", rsp)
	return rsp, nil
}

func (svc *SignerService) ECDH(ctx context.Context,
	req *ECDHRequest) (*ECDHReply, error) {
	LogIt("ECDH req", req)

	node := svc.findNode(req.SelfNodeId)

	// The point coords are little-endian, needs to be converted to big.
	remotePub := lnkey.PubKeyFromRaw(req.Point.Data)

	ss := lnkey.Ecdh(&remotePub, node.PrivKey)

	rsp := &ECDHReply{SharedSecret: &Secret{Data: ss}}
	LogIt("ECDH rsp", rsp)
	return rsp, nil
}

func (svc *SignerService) NewChannel(ctx context.Context,
	req *NewChannelRequest) (*NewChannelReply, error) {
	LogIt("NewChannel req", req)

	_, channel := svc.findNodeChannel(req.SelfNodeId, req.ChannelNonce)

	channel.Capabilities = req.Capabilities

	rsp := &NewChannelReply{}
	LogIt("NewChannel rsp", rsp)
	return rsp, nil
}

func (svc *SignerService) GetPerCommitmentPoint(ctx context.Context,
	req *GetPerCommitmentPointRequest) (*GetPerCommitmentPointReply, error) {
	LogIt("GetPerCommitmentPoint req", req)

	node, _ := svc.findNodeChannel(req.SelfNodeId, req.ChannelNonce)

	// Compute the channel channelSeed for this channel.
	channelSeed := lnkey.HkdfSha256(
		node.ChannelSeedBase,
		req.ChannelNonce,
		[]byte("per-peer seed"), 32)
	var shaseed []byte
	err := lnkey.DeriveBasepoints(channelSeed, nil, nil, nil, &shaseed)
	PanicOnError(err)

	commitPoint := lnkey.PerCommitmentPoint(shaseed, req.N)

	DebugIt("channel seed", hex.EncodeToString(channelSeed))
	DebugIt("shaseed", shaseed)
	DebugIt("per-commit point", commitPoint)

	rsp := &GetPerCommitmentPointReply{
		PerCommitmentPoint: &PubKey{Data: commitPoint.SerializeCompressed()},
	}
	LogIt("GetPerCommitmentPoint rsp", rsp)
	return rsp, nil
}

func (svc *SignerService) SignFundingTx(ctx context.Context,
	req *SignFundingTxRequest) (*SignFundingTxReply, error) {
	LogIt("SignFundingTx req", req)

	node, _ := svc.findNodeChannel(req.SelfNodeId, req.ChannelNonce)

	var tx wire.MsgTx
	rdr := bytes.NewReader(req.Tx.RawTxBytes)
	err := tx.BtcDecode(rdr, wire.ProtocolVersion, wire.LatestEncoding)
	PanicOnError(err)
	DebugIt("SignFundingTx tx", tx)

	hashCache := txscript.NewTxSigHashes(&tx)

	rsp := SignFundingTxReply{}

	for idx := range tx.TxIn {
		childIndex := req.Tx.InputDescs[idx].KeyLoc.KeyIndex
		child, err := node.BIP32Key.Child(uint32(childIndex))
		PanicOnError(err)
		privKey, err := child.ECPrivKey()
		PanicOnError(err)
		pubKey := privKey.PubKey()

		witnessProgram := lnscript.WitnessProgramForPubKey(pubKey, NodeChainParams(node))

		inputValue := req.Tx.InputDescs[idx].Output.Value
		witness, err := txscript.WitnessSignature(&tx, hashCache, idx,
			inputValue, witnessProgram, txscript.SigHashAll, privKey, true)
		if err != nil {
			panic(fmt.Sprintf("txscript.WitnessSignature failed: %v", err))
		}
		rsp.Witnesses = append(rsp.Witnesses, &WitnessStack{Item: witness})
	}

	LogIt("SignFundingTx rsp", rsp)
	return &rsp, nil
}

func (svc *SignerService) SignRemoteCommitmentTx(ctx context.Context,
	req *SignRemoteCommitmentTxRequest) (*SignRemoteCommitmentTxReply, error) {
	LogIt("SignRemoteCommitmentTx req", req)

	node, channel := svc.findNodeChannel(req.SelfNodeId, req.ChannelNonce)

	var tx wire.MsgTx
	rdr := bytes.NewReader(req.Tx.RawTxBytes)
	err := tx.BtcDecode(rdr, wire.ProtocolVersion, wire.LatestEncoding)
	PanicOnError(err)
	DebugIt("SignRemoteCommitmentTx tx", tx)

	// Compute the channel channelSeed for this channel.
	channelSeed := lnkey.HkdfSha256(
		node.ChannelSeedBase,
		req.ChannelNonce,
		[]byte("per-peer seed"), 32)
	DebugIt("channel seed", hex.EncodeToString(channelSeed))

	var localFundingPubkey *btcec.PublicKey
	var secrets lnkey.Secrets
	var shaseed []byte
	var basePoints lnkey.Basepoints
	err = lnkey.DeriveBasepoints(channelSeed, &localFundingPubkey, &basePoints, &secrets, &shaseed)
	PanicOnError(err)

	remoteCommitPoint := lnkey.PubKeyFromRaw(req.RemotePerCommitPoint.Data)

	DebugIt("localFundingPubkey", localFundingPubkey)
	DebugIt("secrets", secrets)
	DebugIt("shaseed", shaseed)
	DebugIt("remote per-commit point", lnkey.DumpRawPubKey(&remoteCommitPoint))
	DebugIt("revocation base point", lnkey.DumpRawPubKey(basePoints.Revocation))
	DebugIt("payment base point", lnkey.DumpRawPubKey(basePoints.Payment))

	info := lnscript.CommitmentInfo{}
	for idx := range tx.TxOut {
		wscript := req.OutputWitscripts[idx]
		if len(wscript) > 0 {
			DebugIt("witscript", wscript)
		}
		out := tx.TxOut[idx]
		// TODO create a validator object
		err = info.HandleOutput(out, wscript)
		if err != nil {
			// Fail here even if policy enforcement is off, since we don't want to pass malformed
			// transactions through further checks
			log.Println("info.HandleOutput failed:", err)
			return nil, ValidationError("Commitment/Output")
		}
	}

	DebugIt("info", info.String())

	if info.HaveToLocal() {
		var err = validateRemoteToLocal(basePoints, remoteCommitPoint, info)
		//noinspection GoBoolExpressions
		if err != nil && policy.Enforce {
			return nil, err
		}
	}

	if info.HaveToRemote() {
		var err = validateRemoteToRemote(shaseed, channel.N, info, basePoints, req.OptionStaticRemotekey)
		if err != nil && channel.N > 0 {
			// Check if we are redoing last commitment
			err = validateRemoteToRemote(shaseed, channel.N-1, info, basePoints, false)
			if err == nil {
				// Decrement, so that increment below bring us back to current value
				// TODO do we need to make sure commitment tx is the same as first call?
				channel.N -= 1
			}
		}
		//noinspection GoBoolExpressions
		if err != nil && policy.Enforce {
			return nil, err
		}
	}

	if len(tx.TxIn) != 1 {
		return nil, ValidationError("Commitment/Input - more than one input")
	}

	LogIt("About to sign at", channel.N)
	channel.N += 1

	idx := 0 // commitment transactions have a single input
	privKey, pubKey := lnkey.DeriveFundingKey(channelSeed)
	hashCache := txscript.NewTxSigHashes(&tx)

	remoteFundingPubKey := lnkey.PubKeyFromRaw(req.RemoteFundingPubkey.Data)
	witnessProgram := lnscript.WitnessProgramForFunding(pubKey, &remoteFundingPubKey)
	inputValue := req.Tx.InputDescs[idx].Output.Value

	DebugIt("witnessProgram", witnessProgram)
	DebugIt("inputValue", inputValue)
	DebugIt("privKey", privKey.Serialize())

	witness, err := txscript.WitnessSignature(&tx, hashCache, idx,
		inputValue, witnessProgram, txscript.SigHashAll, privKey, true)
	if err != nil {
		panic(fmt.Sprintf("txscript.WitnessSignature failed: %v", err))
	}
	rsp := &SignRemoteCommitmentTxReply{
		Signature: &BitcoinSignature{Data: witness[0]},
	}
	LogIt("SignRemoteCommitmentTxReply", rsp)
	return rsp, nil
}

func (svc *SignerService) SignRemoteHTLCTx(ctx context.Context,
	req *SignRemoteHTLCTxRequest) (*SignRemoteHTLCTxReply, error) {
	LogIt("SignRemoteHTLCTx req:", req)

	node, channel := svc.findNodeChannel(req.SelfNodeId, req.ChannelNonce)
	_ = channel // channel unused so far

	var tx wire.MsgTx
	rdr := bytes.NewReader(req.Tx.RawTxBytes)
	err := tx.BtcDecode(rdr, wire.ProtocolVersion, wire.LatestEncoding)
	PanicOnError(err)
	DebugIt("SignRemoteHTLCTx tx", tx)

	for idx := range tx.TxOut {
		out := tx.TxOut[idx]
		LogScript("SignRemoteHTLCTx out", out.PkScript)
		// TODO handleHTLCOutput(out)
	}

	// Compute the channel seed for this channel.
	seed := lnkey.HkdfSha256(node.ChannelSeedBase, req.ChannelNonce, []byte("per-peer seed"), 32)
	DebugIt("channel seed", hex.EncodeToString(seed))

	rsp := &SignRemoteHTLCTxReply{}
	LogIt("SignRemoteHTLCTx rsp:", rsp)
	return rsp, nil
}

func (svc *SignerService) SignMutualCloseTx(ctx context.Context,
	req *SignMutualCloseTxRequest) (*SignMutualCloseTxReply, error) {
	LogIt("SignMutualCloseTx req:", req)

	node, channel := svc.findNodeChannel(req.SelfNodeId, req.ChannelNonce)
	_ = channel // channel unsused so far

	var tx wire.MsgTx
	rdr := bytes.NewReader(req.Tx.RawTxBytes)
	err := tx.BtcDecode(rdr, wire.ProtocolVersion, wire.LatestEncoding)
	PanicOnError(err)
	DebugIt("SignMutualCloseTx tx", tx)

	for idx := range tx.TxOut {
		out := tx.TxOut[idx]
		disasm, _ := txscript.DisasmString(out.PkScript)
		DebugIt("SignMutualCloseTx out", disasm)
		// TODO handleCommitmentOutput(out)
	}

	// Compute the channel seed for this channel.
	seed := lnkey.HkdfSha256(node.ChannelSeedBase, req.ChannelNonce, []byte("per-peer seed"), 32)
	DebugIt("channel seed", hex.EncodeToString(seed))

	// TODO Needs Implementation

	rsp := &SignMutualCloseTxReply{}
	LogIt("SignMutualCloseTx rsp:", rsp)
	return rsp, nil
}

func (svc *SignerService) SignInvoice(ctx context.Context,
	req *SignInvoiceRequest) (*SignInvoiceReply, error) {
	LogIt("SignInvoice req:", req)

	// TODO Needs Implementation

	rsp := &SignInvoiceReply{}
	LogIt("SignInvoice rsp:", rsp)
	return rsp, nil
}

func (svc *SignerService) SignMessage(ctx context.Context,
	req *SignMessageRequest) (*SignMessageReply, error) {
	LogIt("SignMessage req:", req)

	// TODO Needs Implementation

	rsp := &SignMessageReply{}
	LogIt("SignMessage rsp:", rsp)
	return rsp, nil
}

func (svc *SignerService) SignNodeAnnouncement(ctx context.Context,
	req *SignNodeAnnouncementRequest) (*SignNodeAnnouncementReply, error) {
	LogIt("SignNodeAnnouncement req:", req)

	// TODO Needs implementation

	rsp := &SignNodeAnnouncementReply{}
	LogIt("SignNodeAnnouncement rsp:", rsp)
	return rsp, nil
}

func (svc *SignerService) SignChannelAnnouncement(ctx context.Context,
	req *SignChannelAnnouncementRequest) (*SignChannelAnnouncementReply, error) {
	LogIt("SignChannelAnnouncement req:", req)

	// TODO Needs implementation

	rsp := &SignChannelAnnouncementReply{}
	LogIt("SignChannelAnnouncement rsp:", rsp)
	return rsp, nil
}

func (svc *SignerService) SignChannelUpdate(ctx context.Context,
	req *SignChannelUpdateRequest) (*SignChannelUpdateReply, error) {
	LogIt("SignChannelUpdate req:", req)

	// TODO Needs implementation

	rsp := &SignChannelUpdateReply{}
	LogIt("SignChannelUpdate rsp:", rsp)
	return rsp, nil
}

func (svc *SignerService) GetChannelBasepoints(ctx context.Context,
	req *GetChannelBasepointsRequest) (*GetChannelBasepointsReply, error) {
	LogIt("GetChannelBasepoints req:", req)

	// TODO Needs implementation

	rsp := &GetChannelBasepointsReply{}
	LogIt("GetChannelBasepoints rsp:", rsp)
	return rsp, nil
}

func (svc *SignerService) SignCommitmentTx(ctx context.Context,
	req *SignCommitmentTxRequest) (*SignCommitmentTxReply, error) {
	LogIt("SignCommitmentTx req:", req)

	// TODO Needs implementation

	rsp := &SignCommitmentTxReply{}
	LogIt("SignCommitmentTx rsp:", rsp)
	return rsp, nil
}

func (svc *SignerService) SignPenaltyToUs(ctx context.Context,
	req *SignPenaltyToUsRequest) (*SignPenaltyToUsReply, error) {
	LogIt("SignPenaltyToUs req:", req)

	// TODO Needs implementation

	rsp := &SignPenaltyToUsReply{}
	LogIt("SignPenaltyToUs rsp:", rsp)
	return rsp, nil
}

func (svc *SignerService) SignRemoteHTLCToUs(ctx context.Context,
	req *SignRemoteHTLCToUsRequest) (*SignRemoteHTLCToUsReply, error) {
	LogIt("SignRemoteHTLCToUs req:", req)

	// TODO Needs implementation

	rsp := &SignRemoteHTLCToUsReply{}
	LogIt("SignRemoteHTLCToUs rsp:", rsp)
	return rsp, nil
}

func (svc *SignerService) SignDelayedPaymentToUs(ctx context.Context,
	req *SignDelayedPaymentToUsRequest) (*SignDelayedPaymentToUsReply, error) {
	LogIt("SignDelayedPaymentToUs req:", req)

	// TODO Needs implementation

	rsp := &SignDelayedPaymentToUsReply{}
	LogIt("SignDelayedPaymentToUs rsp:", rsp)
	return rsp, nil
}

func (svc *SignerService) SignLocalHTLCTx(ctx context.Context,
	req *SignLocalHTLCTxRequest) (*SignLocalHTLCTxReply, error) {
	LogIt("SignLocalHTLCTx req:", req)

	// TODO Needs implementation

	rsp := &SignLocalHTLCTxReply{}
	LogIt("SignLocalHTLCTx rsp:", rsp)
	return rsp, nil
}

func (svc *SignerService) CheckFutureSecret(ctx context.Context,
	req *CheckFutureSecretRequest) (*CheckFutureSecretReply, error) {
	LogIt("CheckFutureSecret req:", req)

	// TODO Needs implementation

	rsp := &CheckFutureSecretReply{}
	LogIt("CheckFutureSecret rsp:", rsp)
	return rsp, nil
}
