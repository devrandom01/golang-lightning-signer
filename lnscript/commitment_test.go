package lnscript

import (
	"github.com/btcsuite/btcd/btcec"
	"github.com/btcsuite/btcd/txscript"
	"github.com/btcsuite/btcd/wire"
	"testing"
)

var priv1, _ = btcec.NewPrivateKey(btcec.S256())
var priv2, _ = btcec.NewPrivateKey(btcec.S256())

func TestCommitmentInfo_handleToLocalScript_empty(t *testing.T) {
	info := CommitmentInfo{}
	err := info.handleToLocalScript([]byte{})
	if err == nil {
		t.Error("expected error")
	}
}

func TestCommitmentInfo_handleToLocalScript_good(t *testing.T) {
	info := CommitmentInfo{}
	builder := txscript.ScriptBuilder{}
	builder.AddOp(txscript.OP_IF)
	builder.AddData(priv1.PubKey().SerializeCompressed())
	builder.AddOp(txscript.OP_ELSE)
	builder.AddInt64(9)
	builder.AddOp(txscript.OP_CHECKSEQUENCEVERIFY)
	builder.AddOp(txscript.OP_DROP)
	builder.AddData(priv2.PubKey().SerializeCompressed())
	builder.AddOp(txscript.OP_ENDIF)
	builder.AddOp(txscript.OP_CHECKSIG)

	script, err := builder.Script()
	if err != nil {
		t.Error(err)
	}
	err = info.handleToLocalScript(script)
	if err != nil {
		t.Error(err)
	}

	// Handling again should complain of a duplicate
	err = info.handleToLocalScript(script)
	if err == nil {
		t.Error("expected error")
	}

	if !info.HaveToLocal() {
		t.Error("should have to_local")
	}
	if info.HaveToRemote() {
		t.Error("should not have to_remote")
	}
	outScript, _ := PayToWitnessPubKeyHashScript(priv1.PubKey())
	out := wire.TxOut{PkScript: outScript}
	err = info.HandleOutput(&out, nil)
	if err != nil {
		t.Error(err)
	}
	if !info.HaveToLocal() {
		t.Error("should have to_local")
	}
	if !info.HaveToRemote() {
		t.Error("should have to_remote")
	}
}

func TestParsePushInt(t *testing.T) {
	builder := txscript.ScriptBuilder{}
	builder.AddOp(txscript.OP_0)
	script, err := builder.Script()
	if err != nil {
		t.Error(err)
	}
	value, cursor, err := ParsePushInt(script, 0)
	if value != 0 || cursor != 1 {
		t.Error("bad")
	}

	builder = txscript.ScriptBuilder{}
	builder.AddOp(txscript.OP_5)
	script, err = builder.Script()
	if err != nil {
		t.Error(err)
	}
	value, cursor, err = ParsePushInt(script, 0)
	if value != 5 || cursor != 1 {
		t.Error("bad")
	}

	builder = txscript.ScriptBuilder{}
	builder.AddInt64(100)
	script, err = builder.Script()
	if err != nil {
		t.Error(err)
	}
	value, cursor, err = ParsePushInt(script, 0)
	if value != 100 || cursor != 2 {
		t.Error("bad")
	}

	builder = txscript.ScriptBuilder{}
	builder.AddInt64(1000)
	script, err = builder.Script()
	if err != nil {
		t.Error(err)
	}
	value, cursor, err = ParsePushInt(script, 0)
	if value != 1000 || cursor != 3 {
		t.Error("bad")
	}
}
