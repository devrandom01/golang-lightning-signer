package server

import (
	"encoding/hex"
	"testing"

	"github.com/btcsuite/btcd/btcec"
	"github.com/btcsuite/btcd/chaincfg"
	"github.com/btcsuite/btcutil/hdkeychain"

	"gitlab.com/lightning-signer/lightning-signer/lnkey"
)

func TestSignerService_InitHSM(t *testing.T) {
	salt4 := make([]byte, 4)
	secret := make([]byte, 32)

	// Generate the node private key and nodeid.
	pkBytes := lnkey.HkdfSha256(secret, salt4, []byte("nodeid"), 32)
	_, nodePubKey := btcec.PrivKeyFromBytes(btcec.S256(), pkBytes)
	nodeIdSlice := nodePubKey.SerializeCompressed()
	if hex.EncodeToString(nodeIdSlice) != "02058e8b6c2ad363ec59aa136429256d745164c2bdc87f98f0a68690ec2c5c9b0b" {
		t.Error("mismatch")
	}

	chainParams := &chaincfg.TestNet3Params
	bip32Seed := lnkey.HkdfSha256(secret, salt4, []byte("bip32 seed"), 32)
	masterExtKey, err := hdkeychain.NewMaster(bip32Seed, chainParams)
	if err != nil {
		t.Error(err)
	}
	childExtKey, err := masterExtKey.Child(0)
	if err != nil {
		t.Error(err)
	}
	bip32Key, err := childExtKey.Child(0)
	if err != nil {
		t.Error(err)
	}
	if bip32Key.String() != "tprv8ejySXSgpWvEBguEGNFYNcHz29W7QxEodgnwbfLzBCccBnxGAq4vBkgqUYPGR5EnCbLvJE7YQsod6qpid85JhvAfizVpqPg3WsWB6UG3fEL" {
		t.Error("mismatch")
	}

	salt0 := make([]byte, 0)
	csb := lnkey.HkdfSha256(secret, salt0, []byte("peer seed"), 32)
	if hex.EncodeToString(csb) != "ab7f29780659755f14afb82342dc19db7d817ace8c312e759a244648dfc25e53" {
		t.Error("mismatch")
	}
}
