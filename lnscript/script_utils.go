package lnscript

import (
	"bytes"
	"crypto/sha256"
	"errors"
	"github.com/btcsuite/btcd/btcec"
	"github.com/btcsuite/btcd/chaincfg"
	"github.com/btcsuite/btcd/txscript"
	"github.com/btcsuite/btcutil"

	"gitlab.com/lightning-signer/lightning-signer/logging"
)

// CommitScriptToSelf constructs the public key script for the output on the
// commitment transaction paying to the "owner" of said commitment transaction.
// If the other party learns of the preimage to the revocation hash, then they
// can claim all the settled funds in the channel, plus the unsettled funds.
//
// Possible Input Scripts:
//     REVOKE:     <sig> 1
//     SENDRSWEEP: <sig> <emptyvector>
//
// Output Script:
//     OP_IF
//         <revokeKey>
//     OP_ELSE
//         <numRelativeBlocks> OP_CHECKSEQUENCEVERIFY OP_DROP
//         <timeKey>
//     OP_ENDIF
//     OP_CHECKSIG
func CommitScriptToSelf(csvTimeout uint32, selfKey, revokeKey *btcec.PublicKey) ([]byte, error) {
	// This script is spendable under two conditions: either the
	// 'csvTimeout' has passed and we can redeem our funds, or they can
	// produce a valid signature with the revocation public key. The
	// revocation public key will *only* be known to the other party if we
	// have divulged the revocation hash, allowing them to homomorphically
	// derive the proper private key which corresponds to the revoke public
	// key.
	builder := txscript.NewScriptBuilder()

	builder.AddOp(txscript.OP_IF)

	// If a valid signature using the revocation key is presented, then
	// allow an immediate spend provided the proper signature.
	builder.AddData(revokeKey.SerializeCompressed())

	builder.AddOp(txscript.OP_ELSE)

	// Otherwise, we can re-claim our funds after a CSV delay of
	// 'csvTimeout' timeout blocks, and a valid signature.
	builder.AddInt64(int64(csvTimeout))
	builder.AddOp(txscript.OP_CHECKSEQUENCEVERIFY)
	builder.AddOp(txscript.OP_DROP)
	builder.AddData(selfKey.SerializeCompressed())

	builder.AddOp(txscript.OP_ENDIF)

	// Finally, we'll validate the signature against the public key that's
	// left on the top of the stack.
	builder.AddOp(txscript.OP_CHECKSIG)

	return builder.Script()
}

func genMultiSigScript(aPub, bPub []byte) []byte {
	if len(aPub) != 33 || len(bPub) != 33 {
		panic("Pubkey size error. Compressed pubkeys only")
	}

	// Swap to sort pubkeys if needed. Keys are sorted in lexicographical
	// order. The signatures within the scriptSig must also adhere to the
	// order, ensuring that the signatures for each public key appears in
	// the proper order on the stack.
	if bytes.Compare(aPub, bPub) == 1 {
		aPub, bPub = bPub, aPub
	}

	bldr := txscript.NewScriptBuilder()
	bldr.AddOp(txscript.OP_2)
	bldr.AddData(aPub) // Add both pubkeys (sorted).
	bldr.AddData(bPub)
	bldr.AddOp(txscript.OP_2)
	bldr.AddOp(txscript.OP_CHECKMULTISIG)
	result, err := bldr.Script()
	logging.PanicOnError(err)
	return result
}

func WitnessProgramForFunding(localKey *btcec.PublicKey, remoteKey *btcec.PublicKey) []byte {
	return genMultiSigScript(localKey.SerializeCompressed(), remoteKey.SerializeCompressed())
}

func PayToWitnessScriptHashScript(scriptHash []byte) ([]byte, error) {
	return txscript.NewScriptBuilder().AddOp(txscript.OP_0).AddData(scriptHash).Script()
}

func ToLocalScript(toSelfDelay uint32, selfDelayKey *btcec.PublicKey, revokeKey *btcec.PublicKey) []byte {
	toLocalInnerScript, _ := CommitScriptToSelf(toSelfDelay, selfDelayKey, revokeKey)
	scriptHash := sha256.Sum256(toLocalInnerScript)
	toLocalScript, _ := PayToWitnessScriptHashScript(scriptHash[:])
	return toLocalScript
}

func PayToWitnessPubKeyHashScript(publicKey *btcec.PublicKey) ([]byte, error) {
	pubKeyHash := btcutil.Hash160(publicKey.SerializeCompressed())
	toRemoteScript, err := payToWitnessPubKeyHashScript(pubKeyHash)
	return toRemoteScript, err
}

func payToWitnessPubKeyHashScript(pubKeyHash []byte) ([]byte, error) {
	return txscript.NewScriptBuilder().AddOp(txscript.OP_0).AddData(pubKeyHash).Script()
}

var parseError = errors.New("parse error")

func ExpectOpcode(script []byte, cursor int, opcode byte) (int, error) {
	if len(script) <= cursor || script[cursor] != opcode {
		return cursor, parseError
	}
	return cursor + 1, nil
}

func ParseOpData(script []byte, cursor int) ([]byte, int, error) {
	if len(script) <= cursor || script[cursor] < txscript.OP_DATA_1 || script[cursor] > txscript.OP_DATA_75 {
		return nil, cursor, parseError
	}
	length := int(script[cursor] - txscript.OP_DATA_1 + 1)
	if len(script) < cursor+1+length {
		return nil, cursor, parseError
	}
	data := script[cursor+1 : cursor+1+length]
	cursor += 1 + length
	return data, cursor, nil
}

func ParsePushInt(script []byte, cursor int) (int, int, error) {
	if len(script) <= cursor {
		return 0, cursor, parseError
	}
	if script[cursor] == txscript.OP_0 {
		return 0, cursor + 1, nil
	}

	if script[cursor] >= txscript.OP_1 && script[cursor] <= txscript.OP_16 {
		return int(script[cursor] - txscript.OP_1 + 1), cursor + 1, nil
	}
	data, cursor, err := ParseOpData(script, cursor)
	if err != nil {
		return 0, cursor, err
	}
	if len(data) > 2 {
		return 0, cursor, parseError
	}
	if len(data) == 0 {
		return 0, cursor, nil
	}
	if len(data) == 1 {
		return int(data[0]), cursor, nil
	}
	value := (int(data[1]) << 8) + int(data[0])
	return value, cursor, nil
}

func WitnessProgramForPubKey(pubKey *btcec.PublicKey, chainParams *chaincfg.Params) []byte {
	pubKeyHash := btcutil.Hash160(pubKey.SerializeCompressed())
	p2wkhAddr, err := btcutil.NewAddressWitnessPubKeyHash(pubKeyHash, chainParams)
	logging.PanicOnError(err)
	witnessProgram, err := txscript.PayToAddrScript(p2wkhAddr)
	logging.PanicOnError(err)
	return witnessProgram
}
