package liposig

import (
	"fmt"
	"log"
	"net"

	"google.golang.org/grpc"

	"gitlab.com/lightning-signer/lightning-signer/remotesigner"
	"gitlab.com/lightning-signer/lightning-signer/server"
)

// Commit stores the current commit hash of this build, this should be set using
// the -ldflags during compilation.
var Commit string

func Main() error {
	log.Println("liposigd", Commit, "starting")

	port := 50051

	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	grpcServer := grpc.NewServer()

	router, err := server.NewSignerService()
	if err != nil {
		log.Fatalf("new router failed: %v", err)
	}

	remotesigner.RegisterSignerServer(grpcServer, router)
	grpcServer.Serve(lis)

	return nil
}
