module gitlab.com/lightning-signer/lightning-signer

go 1.13

require (
	github.com/btcsuite/btcd v0.20.1-beta
	github.com/btcsuite/btcutil v0.0.0-20190425235716-9e5f4b9a998d
	github.com/cosiner/argv v0.0.1 // indirect
	github.com/go-delve/delve v1.3.2 // indirect
	github.com/go-errors/errors v1.0.1
	github.com/golang/protobuf v1.3.2
	github.com/jessevdk/go-flags v0.0.0-20141203071132-1679536dcc89
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/mattn/go-colorable v0.1.4 // indirect
	github.com/mattn/go-isatty v0.0.10 // indirect
	github.com/mattn/go-runewidth v0.0.6 // indirect
	github.com/peterh/liner v1.1.0 // indirect
	github.com/sirupsen/logrus v1.4.2 // indirect
	github.com/spf13/cobra v0.0.5 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	gitlab.com/ksedgwic/json16 v0.0.0-20191114202027-3b524ec99567
	go.starlark.net v0.0.0-20191113183327-aaf7be003892 // indirect
	golang.org/x/arch v0.0.0-20191101135251-a0d8588395bd // indirect
	golang.org/x/crypto v0.0.0-20191119213627-4f8c1d86b1ba
	golang.org/x/sys v0.0.0-20191120155948-bd437916bb0e // indirect
	google.golang.org/grpc v1.24.0
	gopkg.in/yaml.v2 v2.2.7 // indirect
)
