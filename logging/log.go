package logging

import (
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"github.com/btcsuite/btcd/txscript"
	"gitlab.com/ksedgwic/json16"
	"log"
)

var enableDebug = false

func PanicOnError(e error) {
	if e != nil {
		panic(e)
	}
}

func LogScript(tag string, script []byte) {
	disasm, e := txscript.DisasmString(script)
	PanicOnError(e)
	log.Println(tag, disasm)
}

// Log a JSON representation of an object
func LogIt(tag string, thing interface{}) {
	jj, err := json16.MarshalIndent(thing, "", "    ")
	PanicOnError(err)
	log.Println(tag, string(jj))
}

// Debug log a JSON representation of an object
func DebugIt(tag string, thing interface{}) {
	//noinspection GoBoolExpressions
	if enableDebug {
		jj, err := json16.MarshalIndent(thing, "", "    ")
		PanicOnError(err)
		log.Println(tag, string(jj))
	}
}

func ValidationError(msg string) error {
	log.Println("Validation error", msg)
	return status.Error(codes.InvalidArgument, msg);
}
